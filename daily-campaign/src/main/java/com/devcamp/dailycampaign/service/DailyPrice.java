package com.devcamp.dailycampaign.service;

import java.util.ArrayList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Service;

@Service
public class DailyPrice {
    String stringMon = "Mua 2 combo bất kì tặng 1 combo size S bất kỳ";
    String stringTue = "Mua 1 combo nhận 1 gấu bông";
    String stringWed = "Mua 2 combo bất kì tặng 1 voucher giảm giá 15% cho lần mua tiếp theo";
    String stringThu = "Tặng thêm 1 phần nước uống khi mua mang về";
    String StringFri = "Chsuc quý khách ngon miệng";
    String StringSat = "Tặng miễn phí một phần nước uống";
    String stringSun = "Hóa đơn được giảm 10%";
    public String getDailyPrice(){
        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        String stringResult = "";
        String date = dtfVietnam.format(today);
        switch (date){
            case "Thứ Hai":
                stringResult = stringMon;
                break;
            case "Thứ Ba":
                stringResult = stringTue;
                break;
            case "Thứ Tư":
                stringResult = stringWed;
                break;
            case "Thứ Năm":
                stringResult = stringThu;
                break;
            case "Thứ Sáu":
                stringResult = StringFri;
                break;
            case "Thứ Bảy":
                stringResult = StringSat;
                break;
            case "Chủ Nhật":
                stringResult = stringSun;
                break;
        }
        return String.format("%s, %s.", date, stringResult);
    }
}
