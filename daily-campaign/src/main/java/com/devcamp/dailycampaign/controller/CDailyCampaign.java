package com.devcamp.dailycampaign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.dailycampaign.service.DailyPrice;
@RestController
@RequestMapping("/api")
@CrossOrigin
public class CDailyCampaign {
    @Autowired
    private DailyPrice dailyPrice;
    @GetMapping("/devcamp-date")
    public String getDateVietNam(){
        return dailyPrice.getDailyPrice();
    }
}
