package com.devcamp.dailycampaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyCampaingnApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyCampaingnApplication.class, args);
	}

}
